# Python-symptom-checker-AH


## Introduction
Simple Python-Flask wrapper for the Infremedida Python API. **Python version: 3.6.1**

## Installation
Clone the Github repo:

http:
`$ git clone https://github.com/paulupendo/Python-symptom-checker-AH.git`

cd into the cloned folder and create a [virtual environment](https://virtualenv.pypa.io/en/stable/):

`$ virtualenv venv`

Activate the virtual environment

`$ venv/bin/activate`

Install all app requirements

`$ pip install -r requirements.txt`

Create the database and run migrations
- The database used is Postgres. The application assumes there is existing Patient table and Encounter table with the following fields in a database named: `symptomdb`:

Patient Table: 
> id | name | gender | age | location | risk_fators

Encounter Table:
> id | patient_id | visit_date | symptoms | diagnosis | questions

The database URI is set to: `postgresql://postgres:@localhost/symptomdb` with user postgres having super user privileges. 

To use your own PostgresDB user and database:
`$ cd symptomChecker ` and in the `app.py` file find the line of code that contains this config [line 9] and changes the db user to what you prefer e.g: `postgresql://alemhealth:@localhost/patientdb`

If the tables `Patient` or `Encounter` do not exist you can run the following command tto create the tables:
`$ python manage.py tbinitall`

Use: `$ python manage.py` to get a list of actions you can perform on the database. **NOTE: Be careful with the commands, you might drop your entire database.**

### How to start the API
`$ python run.py` from the project's root or
`$ export FLASK_APP=run.py` then `$ flask run`


### Endpoints:
These are defined in  the `SymptomChecker` folder in the file `patientInfo.py`
The endpoints are listed in the order to which I assume the application should run. **NOTE:** Missing one of the steps might result in an error or two as the API is still in test mode and under development. Following the steps as per Infermedica interview flow will result in no errors. **All endpoints receive their data as form-data and respond with JSON objects.**

Endpoint | Parameters | Functionality
------------ | ------------- | ---------------
POST /patientInfo | id | Receieves patient information from the database by id and send this informationin the response object in JSON format. It also initiates the Diagnosis process and generates an interview id for patient
POST /retrievesymptoms | symptom | Receives patient symptom/ what one feels and sends a list of matching symptoms for the patient to choose from
GET /riskfactors |  No parameters sent | Returns common risk factors that will be useful during the interview process. E.g It is important to know if a patient has blood pressure before beginning the interview
POST /riskfactors | common_factors | Receives a list of risk factor ids gotten from whatever the patient had selected in the GET method of this endpoint. This endpoint adds the risk factors to the symptom list
POST /interview | symptom_id status | Receives symptom_id and symtom status. This can be gotten from the previous patient symptom selection. **NOTE: This is the main diagnosis endpoint. It receives status as either `present`, `unknown` or `absent` as per Infermedica documentation. This endpoint generates interview questions and thus should always be called with patient symptom_id selected and symptom status response. When the probability of a condition existing for a patient gets to 90% the interview process stops and a result it gotten. The info about the patient's encounter/visit/chat-history/interview is stored in the Encounter table as patient profile. This will contain questions asked and disagnosis result.** 
GET /explain | No parameters sent | After a diagnosis result is gotten calling this endpoint gives a  list of supporting and conflicting symptoms that resulted in the patient's diagnosis result
GET /patientProfile | No parameters sent | Retrieves patient profile information in as stored in the database. Will retrieve information about the current patient


### Limitations
- Missing any of the steps above or not following the order will result in an error or crash the backend as error handling has not yet been done.
- Sending wrong symptom status [Anything that's not `present`, `unknown` or `absent`] will crash your backend. No error handling for this has beed done yet.
- Risk factors are not yet saved to the database and querying for patient risk fasctors from the Dabatase is not happening. Patient's need to always select risk factors for now.
- Retrieving patient-profile is regarded as the last step of the whole process for now.
- Nigeria is part of West-Africa but Infermedica API has no support for that partuclar location in the location risk-factors so location is now set to North Africa
- **Kindly reach out with an error you do not find here. NOTE: This is just but the initial release for feedback**





