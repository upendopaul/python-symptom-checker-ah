from symptomChecker.app import app


@app.route('/')
def index():
    return "Welcome to Symptom-checker"


if __name__ == '__main__':
    app.run()
