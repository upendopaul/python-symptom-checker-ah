#!/usr/bin/env python3
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from symptomChecker.app import app, db

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


@manager.command
def dbinit():
    os.system('createdb symptomdb')
    print('Databases created')


@manager.command
def tbinitall():
    db.create_all()
    print('All tables created')


@manager.command
def tbdropall():
    db.drop_all()
    print('All tables dropped')


@manager.command
def dropdb():
    os.system(
        'psql -c "DROP DATABASE IF EXISTS symptomdb"')
    os.system(
        'psql -c "DROP DATABASE IF EXISTS symptomdb"')
    print('Databases dropped')


if __name__ == '__main__':
    manager.run()
