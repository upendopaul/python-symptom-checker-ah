from datetime import datetime
from .app import db


class Patient(db.Model):
    __tablename__ = 'Patient'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(200))
    age = db.Column(db.Integer())
    gender = db.Column(db.String(50))
    location = db.Column(db.String(200))
    risk_factors = db.Column(db.String(1024))

    def __repr__(self):
        """returning a printable version for the object"""
        return "<UserModel: {}>".format(self.gender, self.age)

    def __init__(self, name, age, location, gender, risk_factor):
        self.name = name
        self.location = location
        self.age = age
        self.gender = gender
        self.risk_factors = risk_factors

    def save(self):
        db.session.add(self)
        db.session.commit()


class Encounter(db.Model):
    __tablename__ = 'Encounter'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    patient_id = db.Column(db.Integer(), db.ForeignKey('Patient.id'))
    visit_date = db.Column(db.DateTime, default=datetime.utcnow())
    symptoms = db.Column(db.String(1024))
    diagnosis = db.Column(db.String(1024))
    questions = db.Column(db.String)

    def __init__(self, patient_id, symptoms, diagnosis, questions):
        self.patient_id = patient_id
        self.symptoms = symptoms
        self.diagnosis = diagnosis
        self.questions = questions

    def save(self):
        db.session.add(self)
        db.session.commit()
