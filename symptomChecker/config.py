def setup_api():
    """
    Setup environment to easily run examples.
    API credentials needs to be provided here in order
    to set up api object correctly.
    """
    try:
        import infermedica_api
    except ImportError:
        import sys
        import os

        sys.path.append(os.path.abspath(
            os.path.join(os.path.dirname(__file__), '..')))
        import infermedica_api

    # !!! ENTER YOUR CREDENTIALS HERE !!!
    infermedica_api.configure({
        'app_id': '44d1aee8',
        'app_key': 'b05e4a3700df081f824a5085cb1c2ad2',
        'dev_mode': True  # Use only during development on production remove this parameter
    })