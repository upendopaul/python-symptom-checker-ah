import uuid
import json

import infermedica_api
from flask import jsonify, request
from symptomChecker import config

from .app import app
from .models import Patient, Encounter

config.setup_api()
api = infermedica_api.get_api()


@app.route('/patientInfo', methods=['POST'])
def patientInfo():
    '''
    Retrieves patient data from database and
    appends age and gender to Diagnosis instance
    '''
    global data
    data = request.form['id']
    info = Patient.query.filter_by(id=data).first()
    gender = info.gender
    age = info.age
    if age and gender and gender.strip():
        res_p = {}
        res_p['age'] = age
        res_p['gender'] = gender
        global user_data
        user_data = infermedica_api.Diagnosis(
            sex=gender, age=age, interview_id=str(uuid.uuid4()))
        response = jsonify({'result': res_p})
        response.status_code = 200
        return response


@app.route('/retrievesymptoms', methods=['POST'])
def get_symptoms():
    '''
    searches for matching symptoms from patient input
    '''
    global symptom
    symptom = request.form['symptom']
    symptom_results = search_symptoms(symptom)
    response = jsonify({'result': symptom_results})
    response.status_code = 200
    return response


def search_symptoms(symptoms_str):
    '''
    Outputs more symptoms related to symptom_str (is a list of symptoms)
    user enters
    '''
    search_res = []
    res = api.search(symptoms_str)

    for k in res:
        res_p = {}
        res_p['id'] = str(k[str('id')])
        res_p['label'] = str(k[str('label')])
        search_res.append(res_p)
        res_p = None
    return search_res


@app.route('/riskfactors', methods=['GET'])
def common_riskfactors():
    '''
    Returns a list of common risk factors
    '''
    response = jsonify({'result': get_common_risk_factors()})
    return response


def get_common_risk_factors():
    '''
    Returns common risk factors the patient may posses
    '''
    common_risk_ids = ['p_7', 'p_28', 'p_10', 'p_9', 'p_147', 'p_8']
    common_risks = []
    for factor_id in common_risk_ids:
        res = api.risk_factor_details(factor_id)
        res_p = {}
        res_p['common_name'] = res.common_name
        res_p['id'] = res.id
        common_risks.append(res_p)
    return common_risks


@app.route('/riskfactors', methods=['POST'])
def add_risk_factors():
    '''
    Add risk factors associated with patient
    '''
    common_factors = request.form['common_factors']
    if common_factors and common_factors.strip():
        common_factors = common_factors.split(' ')
        add_common_factors(common_factors)
    response = jsonify(
        {'result': 'successfully added factors to symptoms list'})
    response.status_code = 200
    return response


def add_common_factors(factor_ids):
    '''
    Adds common patient risk factors to patient symptoms
    '''
    for factor_id in factor_ids:
        ids = {}
        ids['id'] = factor_id
        ids['status'] = 'present'
        id_wrapper = []
        id_wrapper.append(ids)
        add_symptoms(id_wrapper)
    return 'success'


@app.route('/interview', methods=['POST'])
def patient_interview():
    '''
    Begins patient interview process
    '''
    try:
        symptom_id = request.form['symptom_id']
        status = request.form['status']
        ids = {}
        ids['id'] = symptom_id
        ids['status'] = status
        id_wrapper = []
        id_wrapper.append(ids)
        user_data.add_symptom('p_16', 'present')  # Add North Africa || Nigeria
        add_symptoms(id_wrapper)
        response = jsonify({'result': get_question()})
        condition = check_risk()
        if condition is 1:
            response = jsonify({'result': get_result()})
            response.status_code = 200
            save_encounter()
            f = open('data.txt', 'w')
            f.truncate()
        return response

    except KeyError:
        response = jsonify({
            'error': 'Bad Request!'
        })
        response.status_code = 400
        return response


@app.route('/explain', methods=['GET'])
def explain():
    '''
    Gets supporting and conflicting symptoms that resulted
    in the diagnosis
    '''
    try:
        response = jsonify({'result': get_explanation()})
        return response
    except Exception:
        response = jsonify({
            'error': Exception
        })
        response.status_code = 400
        return response


def get_explanation():
    '''
    Gets supporting and conflicting symptoms that resulted
    in the diagnosis
    '''
    if user_data.conditions[0][str('id')] and user_data.conditions[0][str('id')].strip():
        res = api.explain(user_data, user_data.conditions[0][str('id')])
        data = {}
        data['supporting_evidence'] = []
        data['conflicting_evidence'] = []
        for i in res.supporting_evidence:
            obj = {}
            obj['id'] = i.id
            obj['name'] = i.name
            obj['common_name'] = i.common_name
            data['supporting_evidence'].append(obj)

        print('HERE', res.conflicting_evidence)

        for i in res.conflicting_evidence:
            objc = {}
            objc['id'] = i.id
            objc['name'] = i.name
            objc['common_name'] = i.common_name
            data['conflicting_evidence'].append(objc)
        return data
    else:
        return 'No diagnosis result gotten yet.'


def add_symptoms(ids):
    '''
    The ids is the list of dicts with keys 'id' and 'status'
    '''
    for i in ids:
        user_data.add_symptom(
            str(i[str(u'id')]),
            str(i[str(u'status')])
        )
    return 'success'


def get_question():
    '''
    Diagnose the previous question/symptoms input
    and gives the next question
    '''
    res = api.diagnosis(user_data)
    optn_list = []
    ques = {}
    ques['text'] = res.question.text
    ques['option'] = []
    condition = check_risk()
    if condition is 0:
        for i in res.question.items:
            optn = {}
            optn['id'] = i['id']
            optn['name'] = i['name']
            optn['choice'] = i['choices']

            ques['option'].append(optn)
        global question_data
        question_data = {}
        question_data['question'] = ques['text']
        print(ques['option'][0]['choice'][0]['label'])
        question_data['answer'] = ques['option'][0]['choice'][0]['label']
        with open('data.txt', 'a') as outfile:
            json.dump(question_data, outfile)
        return ques
    else:
        return 'End of diagnosis'


def check_risk(risk_prob=0.9):
    '''
    Outputs the risk of disease the user may have according
    to the probability risk_prob
    '''
    if user_data.conditions[0]['probability'] > risk_prob:
        return 1
    else:
        return 0


def get_result():
    '''
    Outputs the probable disease the user may have
    '''
    result = {}
    result['id'] = str(user_data.conditions[0][str('id')])
    result['name'] = str(user_data.conditions[0][str('name')])
    result['prob'] = str(user_data.conditions[0]['probability'])
    k = api.condition_details(result['id']).__dict__
    result['hint'] = str(k[str('extras')][str('hint')])
    result['severity'] = str(k[str('severity')])
    result['prevalence'] = str(k[str('prevalence')])
    result['acuteness'] = str(k[str('acuteness')])
    return result


def save_encounter():
    patient_id = data
    symptoms = symptom
    diagnosis = get_result()['name']
    open_file = open('data.txt', 'r')
    visit = Encounter(patient_id=patient_id,
                      symptoms=symptoms,
                      diagnosis=diagnosis,
                      questions=open_file.read())
    visit.save()


@app.route('/patientProfile', methods=['GET'])
def get_patient_profile():
    res = {}

    patient = Patient.query.filter_by(id=data).first()
    encounter = Encounter.query.filter_by(patient_id=data
                                          ).filter_by(symptoms=symptom
                                                      ).first()
    if patient and encounter:
        res['name'] = patient.name
        res['age'] = patient.age
        res['gender'] = patient.gender
        res['date'] = encounter.visit_date
        res['location'] = patient.location
        res['riskfactors'] = patient.risk_factors
        res['symptoms'] = encounter.symptoms
        res['diagnosis'] = encounter.diagnosis
        res['questions'] = encounter.questions
        return jsonify({'result': res})
